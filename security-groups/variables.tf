variable "vpc_id" {}

variable "ingress_description" {
  type    = string
  default = "http acess"
}

variable "ingress_from_port" {
  type    = number
  default = 80
}

variable "ingress_to_port" {
  type    = number
  default = 80
}

variable "ingress_protocol" {
  type    = string
  default = "tcp"
}

variable "second_ingress_description" {
  type    = string
  default = "http acess"
}

variable "second_ingress_from_port" {
  type    = number
  default = 443
}

variable "second_ingress_to_port" {
  type    = number
  default = 443
}

variable "second_ingress_protocol" {
  type    = string
  default = "tcp"
}
