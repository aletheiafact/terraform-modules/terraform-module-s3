variable "name" {
  description = "The name of the security group"
  type        = string
}

variable "description" {
  description = "The description of the security group"
  type        = string
}

variable "vpc_id" {
  description = "The VPC ID"
  type        = string
}

variable "tags" {
  description = "The tags for the security group"
  type        = map(string)
  default     = {}
}

variable "ingress_rules" {
  description = "The ingress rules for the security group"
  type = list(object({
    from_port = number
    to_port   = number
    protocol  = string
    cidr_ipv4 = string
  }))
}

variable "egress_rules" {
  description = "The egress rules for the security group"
  type = list(object({
    from_port = number
    to_port   = number
    protocol  = string
    cidr_ipv4 = string
  }))

  default = [
    {
      from_port = -1
      to_port   = -1
      protocol  = "-1"
      cidr_ipv4 = "0.0.0.0/0"
    }
  ]
}
