variable "name" {
  type        = string
  default     = "Aletheia"
  description = "The project name"
}