resource "aws_resourcegroups_group" "this" {
  name = var.environment

  resource_query {
    query = <<JSON
{
  "ResourceTypeFilters": [
    "AWS::AllSupported"
  ],
  "TagFilters": [
    {
      "Key": "Environment",
      "Values": ["${var.environment}"]
    }
  ]
}
JSON
  }
}

resource "aws_ssm_association" "inventory" {
  name             = "AWS-GatherSoftwareInventory"
  association_name = "GatherSoftwareInventory"

  targets {
    key    = "tag:Environment"
    values = [var.environment]
  }
}

resource "aws_ssm_association" "update_ssm_agent" {
  name             = "AWS-UpdateSSMAgent"
  association_name = "UpdateSSMAgent"

  targets {
    key    = "tag:Environment"
    values = [var.environment]
  }
}
