resource "aws_instance" "this" {
  ami                                  = var.ami
  instance_type                        = var.instance_type
  availability_zone                    = var.availability_zone
  tenancy                              = var.tenancy
  subnet_id                            = var.subnet_id
  ebs_optimized                        = var.ebs_optimized
  vpc_security_group_ids               = var.vpc_security_group_ids
  source_dest_check                    = var.source_dest_check
  monitoring                           = var.monitoring
  iam_instance_profile                 = var.instance_profile
  tags                                 = var.tags
  volume_tags                          = var.tags
  associate_public_ip_address          = var.associate_public_ip_address
  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior


  root_block_device {
    volume_size           = var.volume_size
    volume_type           = var.volume_type
    delete_on_termination = var.delete_on_termination
    encrypted             = var.encrypted
  }
}
