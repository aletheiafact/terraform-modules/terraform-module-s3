variable "ami" {
  description = "The AMI to use"
  type        = string
}

variable "instance_type" {
  description = "The type of instance to start"
  type        = string
  default     = "t2.micro"
}

variable "instance_profile" {
  description = "The name of the instance profile to associate with the instance"
  type        = string
  default     = null
}

variable "user_data" {
  description = "The user data to provide when launching the instance"
  type        = string
  default     = null
}
variable "availability_zone" {
  description = "The availability zone to start the instance in"
  type        = string
}

variable "tenancy" {
  description = "The tenancy of the instance"
  type        = string
  default     = "default"
}

variable "subnet_id" {
  description = "The subnet ID to launch the instance in"
  type        = string
}

variable "ebs_optimized" {
  description = "Whether the launched instance should be EBS-optimized"
  type        = bool
  default     = false
}

variable "vpc_security_group_ids" {
  description = "A list of security group IDs to associate with"
  type        = list(string)
}

variable "source_dest_check" {
  description = "Controls if traffic is routed to the instance when the destination address does not match the instance. Used for NAT or VPNs"
  type        = bool
  default     = true
}

variable "monitoring" {
  description = "Enables detailed monitoring"
  type        = bool
  default     = false
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "volume_size" {
  description = "The size of the EBS volume in gigabytes"
  type        = number
  default     = 8
}

variable "volume_type" {
  description = "The type of EBS volume"
  type        = string
  default     = "gp2"
}

variable "delete_on_termination" {
  description = "Whether the EBS volume should be destroyed on instance termination"
  type        = bool
  default     = true
}

variable "encrypted" {
  description = "Whether the EBS volume should be encrypted"
  type        = bool
  default     = false
}

variable "associate_public_ip_address" {
  description = "Whether the launched instance should have a public IP address"
  type        = bool
  default     = false
}

variable "instance_initiated_shutdown_behavior" {
  description = "Whether the instance initiated shutdown behavior"
  type        = string
  default     = "stop"
}
