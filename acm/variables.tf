variable "domain_name" {
  type = string
}

variable "alternative_name" {
  type = string
}

variable "route53_record_ttl" {
  type    = number
  default = 60
}

variable "route53_private_zone" {
  type    = bool
  default = false
}
