variable "name" {
  description = "The name of the role."
  type        = string
  default     = "my-role"
}

variable "description" {
  description = "The description of the role."
  type        = string
  default     = "My role"
}

variable "path" {
  description = "The path to the role."
  type        = string
  default     = "/"
}

variable "assume_role_policy" {
  description = "The policy that grants an entity permission to assume the role."
  type        = string
}

variable "force_detach_policies" {
  description = "Specifies to force detaching any policies the role has before destroying it."
  type        = bool
  default     = false
}

variable "max_session_duration" {
  description = "The maximum session duration (in seconds) that you want to set for the specified role."
  type        = number
  default     = 3600
}

variable "policy_arns" {
  description = "The ARNs of the managed policies you want to use as managed policies for the role."
  type        = list(string)
}

variable "inline_policy" {
  description = "A list of inline policies to attach to the role."
  type = object({
    name   = string
    policy = string
  })
}
