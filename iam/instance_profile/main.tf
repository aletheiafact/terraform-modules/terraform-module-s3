resource "aws_iam_role" "this" {
  name                  = var.name
  description           = var.description
  path                  = var.path
  assume_role_policy    = var.assume_role_policy
  force_detach_policies = var.force_detach_policies
  max_session_duration  = var.max_session_duration
}

resource "aws_iam_role_policy" "this" {
  name   = var.inline_policy.name
  role   = aws_iam_role.this.name
  policy = var.inline_policy.policy
}

resource "aws_iam_role_policy_attachment" "this" {
  for_each   = toset(var.policy_arns)
  role       = aws_iam_role.this.name
  policy_arn = each.value
}

resource "aws_iam_instance_profile" "this" {
  name = var.name
  path = var.path
  role = aws_iam_role.this.name
}
