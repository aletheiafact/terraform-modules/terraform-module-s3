resource "aws_s3_bucket" "this" {
  bucket = var.name
  tags   = var.tags
}

resource "aws_s3_bucket_versioning" "this" {
  bucket = aws_s3_bucket.this.id

  versioning_configuration {
    status = var.versioning
  }
}

resource "aws_s3_bucket_acl" "this" {
  bucket = aws_s3_bucket.this.id

  access_control_policy {
    grant {
      permission = var.access_control_policy.permission

      grantee {
        type = "CanonicalUser"
        id   = var.access_control_policy.id
      }
    }
    owner {
      id           = var.access_control_policy.id
      display_name = var.access_control_policy.user
    }
  }
}
