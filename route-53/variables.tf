variable "domain_name" {
  default     = "aletheiafact.org"
  description = "domain name"
  type        = string
}

variable "record_name" {
  default     = "www"
  description = "sub domain name"
  type        = string
}

variable "dns_name" {}

variable "zone_id" {}

variable "evaluate_target_health" {
  type    = bool
  default = true
}
