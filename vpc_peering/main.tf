resource "aws_vpc_peering_connection" "this" {
  auto_accept   = var.auto_accept
  peer_owner_id = var.peer_owner_id
  peer_vpc_id   = var.peer_vpc_id
  tags          = var.tags
  vpc_id        = var.vpc_id


  timeouts {
    create = null
    delete = null
    update = null
  }
}

resource "aws_vpc_peering_connection_accepter" "peer" {
  vpc_peering_connection_id = aws_vpc_peering_connection.this.id
  auto_accept               = true
  tags                      = var.tags
}
