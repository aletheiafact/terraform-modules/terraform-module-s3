<!-- BEGIN_AUTOMATED_TF_DOCS_BLOCK -->


## Resources

| Name | Type |
|------|------|
| [aws_vpc_peering_connection.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_peering_connection) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_accepter"></a> [accepter](#input\_accepter) | The requester VPC can communicate with the accepter VPC using private IP addresses | <pre>object({<br>    allow_classic_link_to_remote_vpc = bool<br>    allow_remote_vpc_dns_resolution  = bool<br>    allow_vpc_to_remote_classic_link = bool<br>  })</pre> | <pre>{<br>  "allow_classic_link_to_remote_vpc": false,<br>  "allow_remote_vpc_dns_resolution": false,<br>  "allow_vpc_to_remote_classic_link": false<br>}</pre> | no |
| <a name="input_auto_accept"></a> [auto\_accept](#input\_auto\_accept) | Accept the VPC peering connection automatically | `bool` | `false` | no |
| <a name="input_peer_owner_id"></a> [peer\_owner\_id](#input\_peer\_owner\_id) | The AWS account ID of the owner of the accepter VPC | `string` | n/a | yes |
| <a name="input_peer_vpc_id"></a> [peer\_vpc\_id](#input\_peer\_vpc\_id) | The ID of the VPC with which you are creating the VPC peering connection | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | A mapping of tags to assign to the resource | `map(string)` | `{}` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The ID of the requester VPC | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_vpc_peer_id"></a> [vpc\_peer\_id](#output\_vpc\_peer\_id) | n/a |
<!-- END_AUTOMATED_TF_DOCS_BLOCK -->