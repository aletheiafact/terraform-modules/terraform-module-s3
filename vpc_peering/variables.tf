variable "auto_accept" {
  description = "Accept the VPC peering connection automatically"
  type        = bool
  default     = false
}

variable "peer_owner_id" {
  description = "The AWS account ID of the owner of the accepter VPC"
  type        = string
}

variable "peer_vpc_id" {
  description = "The ID of the VPC with which you are creating the VPC peering connection"
  type        = string
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default     = {}
}
variable "vpc_id" {
  description = "The ID of the requester VPC"
  type        = string
}

variable "accepter" {
  description = "The requester VPC can communicate with the accepter VPC using private IP addresses"
  type = object({
    allow_classic_link_to_remote_vpc = bool
    allow_remote_vpc_dns_resolution  = bool
    allow_vpc_to_remote_classic_link = bool
  })
  default = {
    allow_classic_link_to_remote_vpc = false
    allow_remote_vpc_dns_resolution  = false
    allow_vpc_to_remote_classic_link = false
  }
}
