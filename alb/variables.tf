variable "name" {
  type        = string
  default     = "Aletheia"
  description = "The project name"
}
variable "alb_security_group_id" {}

variable "subnet" {}

variable "subnet_cidr" {}

variable "vpc_id" {}

variable "certificate_arn" {}

variable "health_check_enabled" {
  type    = bool
  default = true
}

variable "health_check_interval" {
  type    = number
  default = 300
}

variable "health_check_path" {
  type    = string
  default = "/"
}

variable "health_check_timeout" {
  type    = number
  default = 60
}

variable "health_check_matcher" {
  type    = number
  default = 200
}

variable "health_check_healthy_threshold" {
  type    = number
  default = 5
}

variable "health_check_unhealthy_threshold" {
  type    = number
  default = 5
}

variable "alb_listener_port" {
  type    = number
  default = 80
}
