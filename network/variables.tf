variable "name" {
  description = "The name for the resource"
  type        = string
  default     = "development"
}

variable "az" {
  description = "The availability zones"
  type        = list(string)
  default = [
    "a",
    "b",
    "c",
  ]
}

variable "vpc_cidr" {
  description = "The VPC CIDR block"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_cidr" {
  description = "The cidr value for public access"
  type        = string
  default     = "0.0.0.0/0"
}

variable "subnets_cidr" {
  description = "The list of subnets CIDRs"
  type        = list(string)
  default = [
    "10.0.0.0/20",
    "10.0.16.0/20",
    "10.0.32.0/20",
  ]
}

variable "routes" {
  type = map(object({
    type       = string
    cidr       = string
    gateway_id = string
    peer_id    = string
  }))

  default = {
    route1 = { type = "igw", cidr = "", gateway_id = "", peer_id = "" }
    route2 = { type = "peer", cidr = "", gateway_id = "", peer_id = "" }
  }
}
