module "s3" {
  source = "./s3"
  name   = "mock-bucket"
  acl    = "private"
  tags = {
    "Environment" = "Mock"
    "ManagedBy"   = "Terraform"
  }
}

module "network" {
  source       = "./network"
  name         = "mock-vpc"
  az           = ["a", "b", "c"]
  vpc_cidr     = "100.0.0.0/16"
  subnets_cidr = ["100.0.1.0/20", "100.0.2.0/20", "100.0.3.0/20"]
}

module "ecr" {
  source = "./ecr"
  name   = "mock-registry"
}

module "alb" {
  source                = "./alb"
  name                  = "aletheia-alb"
  alb_security_group_id = module.security_groups.alb_security_group_id
  subnet                = module.network.subnet
  subnet_cidr           = module.network.subnet_cidr
  vpc_id                = module.network.vpc_id
  certificate_arn       = module.acm.certificate_arn
}

module "security_groups" {
  source = "./security-groups"
  vpc_id = module.network.vpc_id
}

module "acm" {
  source           = "./acm"
  domain_name      = "aletheiafact.org"
  alternative_name = "*.aletheiafact.org"
}

module "route-53" {
  source   = "./route-53"
  dns_name = module.alb.application_load_balancer_dns_name
  zone_id  = module.alb.application_load_balancer_zone_id
}

module "waf" {
  source = "./waf"
}
