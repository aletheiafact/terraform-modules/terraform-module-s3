variable "web_acl_name" {
  type    = string
  default = "aletheiawebacl"
}
variable "web_acl_metics" {
  type    = string
  default = "aletheiawebaclmetics"
}
variable "waf_rule_name" {
  type    = string
  default = "aletheiawafrulename"
}
variable "waf_rule_metrics" {
  type    = string
  default = "aletheiawafrulemetrics"
}
variable "waf_rule_group_name" {
  type    = string
  default = "aletheiawaf_rule_group_name"
}
variable "waf_rule_group_metrics" {
  type    = string
  default = "aletheiawafrulgroupmetrics"
}

variable "waf_ipset_name" {
  type    = string
  default = "Aletheiaipset"
}

variable "ip_set_descriptors_type" {
  type    = string
  default = "IPV4"
}

variable "ip_set_descriptors_value" {
  type    = string
  default = "10.111.0.0/20"
}
